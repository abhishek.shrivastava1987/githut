<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GitHutController extends Controller {

    /**
     * @Route("/", name="githut")
     */
    public function githutAction(Request $request)
    {
        return $this->render('githut/index.html.twig',[
            'avatar_url'    =>  'https://avatars2.githubusercontent.com/u/12968163?v=4',
            'name'          =>  'Code Review Videos',
            'login'         =>  'codereviewvideos',
            'details'       =>  [
                'company'   =>  'Code Review Videos',
                'location'  =>  'Preston, Lancs, UK',
                'joined_on' =>  'Joined On 3rd Jan 2015'
            ],
            'blog'          =>  'https://codereviewvideos.com/',
            'social_data'   =>  [
                'Public Repos'  =>	'53',
                'Public Gists'  =>	'2',
                'Followers'     =>	'66',
                'Following'     =>	'2'
            ],
            'repo_count'    =>  100,
            'most_stars'    =>  67,
            'repos'     =>  [
                [
                    'name'  =>  'First Repo',
                    'url'   =>  'https://google.com',
                    'stargazers_count'  =>  46,
                    'description'   =>  ""
                ],
                [
                    'name'  =>  'Second Repo',
                    'url'   =>  'https://google.com',
                    'stargazers_count'  =>  46,
                    'description'   =>  ""
                ],
                [
                    'name'  =>  'Third Repo',
                    'url'   =>  'https://google.com',
                    'stargazers_count'  =>  46,
                    'description'   =>  ""
                ]
            ],
        ]);
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profileAction(Request $request)
    {
        return $this->render(':githut:profile.html.twig', [
            'avatar_url'    =>  'https://avatars2.githubusercontent.com/u/12968163?v=4',
            'name'          =>  'Code Review Videos',
            'login'         =>  'codereviewvideos',
            'details'       =>  [
                'company'   =>  'Code Review Videos',
                'location'  =>  'Preston, Lancs, UK',
                'joined_on' =>  'Joined On 3rd Jan 2015'
            ],
            'blog'          =>  'https://codereviewvideos.com/',
            'social_data'   =>  [
                'Public Repos'  =>	'53',
                'Public Gists'  =>	'2',
                'Followers'     =>	'66',
                'Following'     =>	'2'
            ],
        ]);
    }
}